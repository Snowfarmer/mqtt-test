import paho.mqtt.client as mqtt
import time #to close loop

#create callback function
def on_message(client, userdata, message):
    print("message received ", str(message.payload.decode("utf-8")))
    print("message topic = ", message.topic)
    print("message qos = ", message.qos)
    print("message retain flag = ", message.retain)


#get broker address
broker_address = "mqtt.eclipseprojects.io"

#create client instance
client = mqtt.Client(client_id="rhi123_test_1")

#attach function
client.on_message = on_message

#connect to broker
client.connect(broker_address)
print("connecting to broker")

#start the loop
client.loop_start()

#subscribe to topic
print("Subscribing to topic")
client.subscribe("rhimqtttest123")

#publish message
print("publishing topic")
client.publish("rhimqtttest123", "TestingTopics")

time.sleep(4)
#stop loop
client.loop_stop()