
//create client
var mqtt = require('mqtt')
var clientID = 'rhi_1234_NodeJS'
var topic = 'rhi1234/nodeJSTopic'

var client = mqtt.connect('mqtt://test.mosquitto.org', {
    clientID,
    connectTimeout: 4000,
    reconnectPeriod: 1000,
})

client.on('connect', () => {
    console.log('Server Connected')
    client.subscribe(topic, () =>{
        console.log ('Subscribed to topic')
    })
})

client.on('message', (topic, payload) => {
    console.log('Received Mesage:', topic, payload.toString())
})

client.on('connect', () => {
    client.publish(topic, ' Hello Testing Payload Stuff!', {qos: 0, retain: false }, (error) => {
        if (error) {
            console.error(error)
        }
    })
})