package com.mqtttest.app

import java.awt.BorderLayout as BL
import javax.swing.*
import java.awt.*
import java.awt.event.ActionListener

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.nio.charset.StandardCharsets;

//Create a frame for Mqtt sub messages

//create JFrame
JFrame client = new JFrame("Subscriber")

client.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
client.setSize(700,300)

//create interactive
JPanel panel = new JPanel()
JLabel tLabel = new JLabel("Enter Topic: ")
JTextField topicField = new JTextField(10)
JButton send = new JButton ("Subscribe")

//add to panel
panel.setLayout(new FlowLayout())
panel.add(tLabel)
panel.add(topicField)
panel.add(send)


//Make text area
JTextArea ta = new JTextArea()

//Add to frame
client.getContentPane().add(BL.SOUTH, panel)
client.getContentPane().add(BL.CENTER, ta)
client.setVisible(true)

//mqtt stuff
String topic = ""
String broker = "tcp://mqtt.eclipseprojects.io:1883"
MemoryPersistence persistence = new MemoryPersistence()

try{
    MqttClient sampleClient = new MqttClient(broker, "rhisub", persistence)
    MqttConnectOptions options = new MqttConnectOptions()
    options.setCleanSession(true)
    System.out.println("Begin connection to broker")
    sampleClient.connect(options)
    System.out.println("Connected")
    //action listener
    send.addActionListener({ topic = topicField.text
        //Subscribe
        System.out.println("Subscribe to topic")
        sampleClient.subscribe(topic, 0)} as ActionListener)

    //Get Message
    while (true){
        sampleClient.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable throwable) {
                System.out.println("Connection to broker lost" + throwable.getMessage())
            }

            @Override
            public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
                ta.text ="Recieved Message!" +
                        "\n\tTopic:      " + s +
                        "\n\tMessage:    "+ mqttMessage
                System.out.println("Recieved Message!" +
                        "\n\tTopic:      " + s +
                        "\n\tMessage:    "+ mqttMessage)
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

            }
        })
    }
}

catch(MqttException me)
{
    System.out.println("reason "+me.getReasonCode());
    System.out.println("msg "+me.getMessage());
    System.out.println("loc "+me.getLocalizedMessage());
    System.out.println("cause "+me.getCause());
    System.out.println("excep "+me);
    me.printStackTrace();
}

