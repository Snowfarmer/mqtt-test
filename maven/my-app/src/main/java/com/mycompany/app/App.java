package com.mycompany.app;

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.nio.charset.StandardCharsets;

public class App 
{
    public static void main( String[] args )
    {
        String broker = "tcp://mqtt.eclipseprojects.io:1883";
        String clientId = "javasample";
        MemoryPersistence persistence = new MemoryPersistence();

        String content = "Testing Messages";

        try{
            MqttClient sampleClient = new MqttClient(broker, clientId, persistence);
            MqttConnectOptions options = new MqttConnectOptions();
            options.setCleanSession(true);
            System.out.println("Begin connection to broker");
            sampleClient.connect(options);
            System.out.println("Connected");
            //Get Message
            sampleClient.setCallback(new MqttCallback() {
                @Override
                public void connectionLost(Throwable throwable) {
                    System.out.println("Connection to broker lost" + throwable.getMessage());
                }

                @Override
                public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
                    System.out.println("Recieved Message!" +
                            "\n\tTopic:      " + s +
                            "\n\tMessage:    "+ mqttMessage);
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

                }
            });
            //Subscribe
            System.out.println("Subscribe to topic");
            sampleClient.subscribe("Rhi1234Javatest", 0);

            System.out.println("Publish Message");
            MqttMessage message = new MqttMessage(content.getBytes());
            message.setQos(1);
            sampleClient.publish("Rhi1234Javatest", message);
            System.out.println("Published Message");
            sampleClient.disconnect();
        }
        catch(MqttException me)
        {
            System.out.println("reason "+me.getReasonCode());
            System.out.println("msg "+me.getMessage());
            System.out.println("loc "+me.getLocalizedMessage());
            System.out.println("cause "+me.getCause());
            System.out.println("excep "+me);
            me.printStackTrace();
        }


    }
}
