package com.mqtttest.app

import java.awt.BorderLayout as BL
import javax.swing.*
import java.awt.*
import java.awt.event.ActionListener

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence

//Frame
JFrame pub = new JFrame("Publisher")

pub.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
pub.setSize(350, 200)

//interactive
JPanel toppanel = new JPanel()
JPanel botpanel = new JPanel()
JLabel tLabel = new JLabel("Enter Topic: ")
JLabel pLabel = new JLabel("Enter Payload: ")
JTextField topicField = new JTextField(20)
JTextField payField = new JTextField(20)
JButton send = new JButton ("Send")

//add to panels
toppanel.setLayout(new FlowLayout())
toppanel.add(tLabel)
toppanel.add(topicField)

botpanel.setLayout(new FlowLayout())
botpanel.add(pLabel)
botpanel.add(payField)

//add to frame
pub.getContentPane().add(BL.NORTH, toppanel)
pub.getContentPane().add(BL.CENTER, botpanel)
pub.getContentPane().add(BL.SOUTH, send)
pub.setVisible(true)

//MQTT CONNECTION
String topic = ""
String payload = ""
String broker = "tcp://mqtt.eclipseprojects.io:1883"
MemoryPersistence persistence = new MemoryPersistence()

try {

    //connection
    MqttClient pubClient = new MqttClient(broker, "rhipub", persistence)
    MqttConnectOptions options = new MqttConnectOptions()
    options.setCleanSession(true)
    System.out.println("Begin connection to broker")
    pubClient.connect(options)
    System.out.println("Connected")
    //action listener
    send.addActionListener({ topic = topicField.text
        payload = payField.text
        //publsih
        System.out.println("Publish message")
        MqttMessage message = new MqttMessage(payload.getBytes())
        message.setQos(1)
        pubClient.publish(topic, message)} as ActionListener)
}
catch(MqttException me)
{
    System.out.println("reason "+me.getReasonCode());
    System.out.println("msg "+me.getMessage());
    System.out.println("loc "+me.getLocalizedMessage());
    System.out.println("cause "+me.getCause());
    System.out.println("excep "+me);
    me.printStackTrace();
}
