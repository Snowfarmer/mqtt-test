﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;

namespace mqttNetM2MQTT
{
    class Program
    {
        static void Main(string[] args)
        {
            MqttClient client = new MqttClient("test.mosquitto.io");
            client.MqttMsgPublishReceived += client_MqttMsgPublishReceived;
        }
    }
}
