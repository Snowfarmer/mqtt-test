﻿using System;
using System.Threading.Tasks;
using System.Threading;
using System.Text;
using System.Net;
using MQTTnet;
using MQTTnet.Client.Options;
using MQTTnet.Client;
using MQTTnet.Client.Subscribing;

namespace mqttNetFrameWork
{
    class Program
    {
        static async Task Main(string[] args)
        {
            // Create a new MQTT client.
            var factory = new MqttFactory();
            var mqttClient = factory.CreateMqttClient();

            //connect to broker
            var options = new MqttClientOptionsBuilder()
                .WithClientId("rhi123test")
                .WithTcpServer("test.mosquitto.org")
                .Build();

            await mqttClient.ConnectAsync(options, CancellationToken.None);
            Console.WriteLine("Connected");


            mqttClient.UseDisconnectedHandler(async e =>
            {
                Console.WriteLine("Disconnected from broker");
                await Task.Delay(TimeSpan.FromSeconds(5));

                try
                {
                    await mqttClient.ConnectAsync(options, CancellationToken.None); // Since 3.0.5 with CancellationToken
                }
                catch
                {
                    Console.WriteLine("Re connection failed");
                }
            });

            // Recieving message
            mqttClient.UseApplicationMessageReceivedHandler(e =>
            {
                Console.WriteLine("Recieving message");
                Console.WriteLine($"+ Topic = {e.ApplicationMessage.Topic}");
                Console.WriteLine($"+ Payload = {Encoding.UTF8.GetString(e.ApplicationMessage.Payload)}");
                Console.WriteLine($"+ QoS = {e.ApplicationMessage.QualityOfServiceLevel}");
                Console.WriteLine();

                Task.Run(() => mqttClient.PublishAsync("hello/world"));
            });

            mqttClient.UseConnectedHandler(async e =>
            {
                Console.WriteLine("Subscribing to topic");

                // Subscribe to a topic
                await mqttClient.SubscribeAsync(new MqttClientSubscribeOptionsBuilder().WithTopicFilter("hello/world").Build());

                Console.WriteLine("Subscribed");
            });

            Console.WriteLine("Making Message");

            var message = new MqttApplicationMessageBuilder()
                    .WithTopic("hello/world")
                    .WithPayload("Howdey, my name is flowey the flower")
                    .Build();
            Console.WriteLine("Message Published");
        }        
    }
}
